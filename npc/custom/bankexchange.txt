//||------------------------------------------------------------------------------||
//||---------------------Simple Bank Note NPC ------------------------------------||
//||---------------------------by: Whathell---------------------------------------||
//||-----------------------------Additions----------------------------------------||
//||---------------------------by: Scrubtasticx-----------------------------------||

amatsu,211,261,5	script	Bank Exchange	833,{

//|=====================Settings==============================
set @npcname$,"^ff0000[Bank Exchange]^000000"; //npc name
set @banknoteprice,1000000; //price you want for each bank note
set @banknoteid,7922;	//the item id for your bank note
set @maxzeny,2000000000; //put in the max zeny you have for your server
//|===========================================================
	mes @npcname$;
	mes "Hi "+ strcharinfo(0) +", I can change your bank notes into zeny, and vice versa.";
	mes "Each bank note is worth 1 Million Zeny. You currently have [^0000FF" + #banknoteid + "^000000] Bank Notes.";
	next;
	mes @npcname$;
	mes "So tell me,what can I do for you?";
		switch(select("Exchange Bank Notes to Zeny:Exchange Zeny to Bank Notes")) {
	case 1:
	next;
	mes @npcname$;
	mes "Please put in the amount of Bank Notes you want to exchange:";
	input @notedeposit;
	next;
	set deposit,@notedeposit;
	
		if (@notedeposit <= 0) {
		mes @npcname$;
		mes "Please come back if you change your mind.";
		close;
		} else if (#banknoteid < deposit) {
		mes @npcname$;
		mes "You dont have enough Bank Notes to complete this transaction.";
		close;
		} else if ((@banknoteamount*@banknoteprice)+Zeny > @maxzeny) {
		mes @npcname$;
		mes "You cannot exchange this number of Bank Notes because you will exceed the maximum amount of zeny you can hold.";
		close;
		}
		next;
		set @price,@banknoteprice*@notedeposit;
		set #banknoteid,#banknoteid - @notedeposit;
		set Zeny,Zeny+@price;
		mes @npcname$;
	mes "Transaction complete, you currently have [^0000FF" + #banknoteid + "^000000] Bank Notes.";
	break;
	case 2:
	next;
	mes @npcname$;
	mes "Please put in the amount of Bank Notes you want.";
	next;
	input @notewithdraw;
		if (@notewithdraw <= 0) {
		mes @npcname$;
		mes "Please come back if you change your mind.";
		close;
		} else if (@notewithdraw*@banknoteprice > Zeny) {
		mes @npcname$;
		mes "I'm sorry but you lack Zeny to complete this transaction.";
		close;
	}
	next;
	set @pricea,@notewithdraw*@banknoteprice;
	set Zeny,Zeny-@pricea;
	set #banknoteid,#banknoteid + @notewithdraw;
	mes @npcname$;
	mes "Transaction complete, you currently have [^0000FF" + #banknoteid + "^000000] Bank Notes.";
	break;
	}
	close;
	end;
	}